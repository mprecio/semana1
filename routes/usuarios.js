var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuario');

router.get('/', usuarioController.list);
router.post('/create', usuarioController.create);
router.get('/create', usuarioController.create_get)
router.post('/:id/delete', usuarioController.delete);
router.post('/:id/update', usuarioController.update);
router.get('/:id/update', usuarioController.update_get);

module.exports = router;