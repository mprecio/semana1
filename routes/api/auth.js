var express = require('express');
var passport = require('passport')
var router = express.Router();

var authController = require('../../controllers/api/authControllerAPI');

router.post('/autenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), authController.authFecebookToken);

module.exports = router;

