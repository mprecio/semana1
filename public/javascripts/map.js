
var map = L.map('main_map', {
    center: [-34.5873,-56.32555],
    zoom: 13
});


L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

/*
L.marker([-34.5873,-56.32555]).addTo(map);
L.marker([-34.54473,-56.34555]).addTo(map);
L.marker([-34.5773,-56.33555]).addTo(map);
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})