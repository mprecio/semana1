require('newrelic');
require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const passport = require('./config/passport');
const session = require('express-session');
const mongoDBSStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletaRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');

var bicicletaAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');

let store;
if(process.env.MODE_ENV === 'develpment'){
  store = new session.MemoryStore;
}else{
  store = new mongoDBSStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });

  store.on('errr', function(err){
    assert.ifError(err);
    assert.ok(false);
  });
}


var app = express();

app.set('secretKey', 'jwt_pwd_!!2233431214');

app.use(session({
  cookie : {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: 'red_bicicletas_pdas932932id·"·$&/&(/&(&"123'  
}));

const Usuario = require('./models/usuario');
const Token = require('./models/token');


//MONGODB
var mongoose = require('mongoose');
const { assert } = require('console');
//var mongodb = 'mongodb://root:admin@localhost:27017/red_bicicleta?authSource=admin';
//var mongodb = 'mongodb+srv://m0dIYQfqxb3WWI6v:m0dIYQfqxb3WWI6v@cluster0.vtiv1.mongodb.net/test';
var mongodb = process.env.MONGO_URI; 
mongoose.connect(mongodb,{useNewUrlParser: true,  useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error: '));



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req,res){
  res.render('sessions/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('sessions/login', {info});

    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('sessions/forgotPassword')
});

app.post('/forgotPassword', function (req, res) {
  Usuario.findOne({ email: req.body.email }, function(err, user) {
      if (!user) return res.render('sessions/forgotPassword', { info: { message: 'No existe el email para un usuario existente' } });

      user.resetPassword(function(err) {
          if (err) return next(err);
          console.log('sessions/forgotPasswordMessage');
      });

      res.render('sessions/forgotPasswordMessage');
  }); 
});

app.get('/resetPassword/:token', function(req, res, next) {
  console.log(req.params.token);
  Token.findOne({ token: req.params.token }, function(err, token) {
      if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado' });
      Usuario.findById(token._userId, function(err, user) {
          if(!user) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
          res.render('sessions/resetPassword', {errors: { }, usuario: user});
      });
  });
});

app.post('/resetPassword', function(req, res) {
  if(req.body.password != req.body.confirm_password) {
      res.render('sessions/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, user: new User({email: req.body.email})});
      return;
  }
  Usuario.findOne({email: req.body.email}, function(err, user) {
      user.password = req.body.password;
      user.save(function(err) {
          if(err) {
              res.render('sessions/resetPassword', {errors: err.errors, user: new Usuario({email: req.body.email})});
          } else {
              res.redirect('/login');
          }
      });
  });
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletaRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/api/bicicletas', validarUsuario, bicicletaAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);
app.use('/api/auth', authAPIRouter);

app.use('/privacy_policy', function(req, res){
  res.sendfile('public/privacy_policy.html');
});

app.use('/google42f2f5d0c4d25053', function(req, res){
  res.sendfile('public/google42f2f5d0c4d25053.html');
});


app.get('/auth/google',
  passport.authenticate('google', { scope:
      [ ' email', 'profile' ] }
));

app.get( '/auth/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));
/*
app.post('/auth/facebook/token',
  passport.authenticate('facebook-token'),
  function (req, res) {
    // do something with req.user
    res.send(req.user? 200 : 401); 
  }
);
*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next();
  }else{
    console.log('Usuario sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded){
      if(err) {
          res.json({status:"error", message:err.message, data:null});
      }else{
          req.body.userId = decoded.id;
          console.log('jwt verify ' + decoded);
          next();
      }
  })
}

module.exports = app; 
